package dev.game.project;
import java.util.Scanner;

public class Game {

    public Game() {
        super();
    }

    public void quiz(Scanner in, String fName, Player player) {
        System.out.println("Can you guess what food item this is? ");

        String answer = in.nextLine();

        //validate answer
        if (fName.equalsIgnoreCase(answer)) {
            System.out.println("correct it was " + fName + "\n");
            player.addPoints();
        } else {
            System.out.println("Im sorry, It was really " + fName + "\n");
        }
    }

    public String gameStart() {
        Scanner in = new Scanner(System.in);
        System.out.println("Who are you? ");
        String pName = in.nextLine();
        return pName;
    }

    public void gameOver(String pName, int points) {
        if (points > 3) {
            System.out.println("Nice, " + pName + " was able to answer more than half of the questions. \nYou got " + points + " out of 6 questions");
        } else {
            System.out.println("Im sorry " + pName + ", you weren't able to answer more than half of the questions. \nYou got " + points + " out of 6 questions");
        }
    }


}
