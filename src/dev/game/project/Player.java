package dev.game.project;

public class Player {
    private String name;
    private int points = 0;
				//added comment
    public Player() { super(); }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints() {
        points++;
    }


}
