package dev.food.project;

public class Fruit extends Food {

    private boolean hasPit;

    Fruit() { super(); }

    Fruit(boolean hasPit) {
        this.hasPit = hasPit;
    }

    Fruit(String name, double calories, String color, boolean hasPit) {
        super(name, calories, color);
        this.hasPit = hasPit;
    }

    public void setHasPit(boolean hasPit) {
        this.hasPit = hasPit;
    }

    public void printString() {
        System.out.println("-----------------------------------------------------");
        System.out.println("\tThis Fruit is " + this.color + " and has " + this.calories + " calories \n" + "The statement \"this fruit has a pit\" is " + this.hasPit + "\n");
    }

}
