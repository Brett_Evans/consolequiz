package dev.food.project;

public class Meat extends Food {

    private boolean hasBone;

    Meat() { super(); }

    Meat(boolean hasBone) {
        this.hasBone = hasBone;
    }

    Meat(String name, double calories, String color, boolean hasBone) {
        super(name ,calories, color);
        this.hasBone = hasBone;
    }

    public void setHasBone(boolean hasBone) {
        this.hasBone = hasBone;
    }

    public void printString() {
        System.out.println("-----------------------------------------------------");
        System.out.println("\tThis Meat is " + this.color + " and has " + this.calories + " calories \n" + "The statement \"this piece of meat has a bone\" is " + this.hasBone + "\n");
    }

}
