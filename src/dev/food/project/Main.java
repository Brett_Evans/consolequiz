package dev.food.project;

import dev.game.project.Game;
import dev.game.project.Player;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Important game objects
        Scanner in = new Scanner(System.in);                    //This is the Scanner; it takes user input
        Game game = new Game();                                 //The game object handles the "quiz" input
        Player player = new Player();                           //The player
        player.setName(game.gameStart());

        //Food Objects
        Meat tBoneSteak = new Meat("steak",385.4, "Red", true);
        Meat chickenTender = new Meat("chicken", 114.3, "White", false);
        Meat salmon = new Meat("salmon", 34.4, "Pink", true);
        Fruit watermelon = new Fruit("watermelon", 46, "Pink", false);
        Fruit grannySmith = new Fruit("apple", 14, "Green", true);
        Fruit grapes = new Fruit(false);
        grapes.calories = 2;
        grapes.color = "Purple";
        grapes.name = "grape";

        // From here down is the quiz game
        tBoneSteak.printString();
        game.quiz(in, tBoneSteak.name, player);

        chickenTender.printString();
        game.quiz(in, chickenTender.name, player);

        salmon.printString();
        game.quiz(in, salmon.name, player);

        watermelon.printString();
        game.quiz(in, watermelon.name, player);

        grannySmith.printString();
        game.quiz(in, grannySmith.name, player);

        grapes.printString();
        game.quiz(in, grapes.name, player);

        game.gameOver(player.getName(), player.getPoints());

        in.close();
    }
}
