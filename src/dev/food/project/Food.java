package dev.food.project;

public abstract class Food {
    double calories;
    String color;
    String name;

    Food() { super(); }

    Food(String name, double calories, String color){
        this.name = name;
        this.calories = calories;
        this.color = color;
    }

    public void takeCalories(double calories) {
        this.calories -= calories;
    }
}
